
:- table naive_shortest/3, naive_path_pref/2, naive_path/1, naive_winnow/1, naive_bypassed/1,
         dag_shortest/3, dag_bypassed/1, dag_path/1, gen_shortest/4, gen_bypassed/1, gen_path/1.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

naive_shortest(X,Y,C):-naive_winnow((X,Y,C)).

naive_path_pref((X,Y,C1),(X,Y,C2)) :- 
  naive_path((X,Y,C1)), naive_path((X,Y,C2)), C1 < C2.

naive_path((X,Y,C)) :- edge((X,Y,C)).
naive_path((X,Y,C)) :- edge((X,Z,A)), 
  naive_path((Z,Y,B)), C is A+B.
  
naive_winnow(T) :- naive_path(T), not naive_bypassed(T).
naive_bypassed(T) :- naive_path_pref(Z,T),naive_path(Z).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dag_shortest(X,Y,C):-dag_path((X,Y,C)), not dag_bypassed((X,Y,C)).

dag_bypassed((X,Y,C2)) :- dag_path((X,Y,C1)),C1 < C2.

dag_path((X,Y,C)):-edge((X,Y,C)).
dag_path((X,Y,C)):-edge((X,Z,C1)),dag_path((Z,Y,C2)), 
                   not dag_bypassed((Z,Y,C2)),C is C1+C2.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_shortest(X,Y,C,N):-gen_path((X,Y,C,N)), not gen_bypassed((X,Y,C,N)).

gen_bypassed((X,Y,C2,N)) :- gen_path((X,Y,C1,N)),C1<C2.

gen_path((X,Y,C,N)):-N>0,edge((X,Y,C)).
gen_path((X,Y,C,N)):-N>1,edge((X,Z,C1)),N1 is N-1,gen_path((Z,Y,C2,N1)),
                     not gen_bypassed((Z,Y,C2,N1)),C is C1+C2.
                     