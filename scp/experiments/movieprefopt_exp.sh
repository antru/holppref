
source conf.cfg

EXPERIMENT="optim"

DEPENDENCIES="movieprefopt"

SOURCEDIR="../"

DATAFILES="
movie_100
movie_500
movie_1000
movie_2000
movie_4000
movie_8000
movie_10000
"

CALLS="
winnow_c1
winnow_c2
winnow_c3
winnow_conj
winnow_prior
winnow_director
"

###############################################################################

rm $SOURCEDIR/src/*.xwam
rm $SOURCEDIR/data/*.xwam

for DATAFILE in `echo $DATAFILES`
do
  # initialize consult string
  
  echo -n "[" > consult.tmp
  echo "$DEPENDENCIES" | xargs -I{} echo -n "'"$SOURCEDIR"src/"{}"'," >> consult.tmp
  echo "'"$SOURCEDIR"data/"$DATAFILE"']." >> consult.tmp
  
  echo 'Experiment,Datafile,Call,Time,Results' > results.csv
  
  for CALL in `echo $CALLS`
  do
    START=$(($(date +%s%N)/1000000))

    echo $CALL"(X),write(X),nl,fail." > command.tmp
    cat consult.tmp command.tmp | $XSB_PATH | tee $CALL.output.tmp

    END=$(($(date +%s%N)/1000000))

    TIME=$(($END-$START))
    RESULTS=$(($(cat $CALL.output.tmp | wc -l)-2))
    
    CALLPRINT=`echo $CALL | tr ",;" "_"`

    echo $EXPERIMENT","$DATAFILE","$CALLPRINT","$TIME","$RESULTS >> results.csv
  done
  
  # move to a backup directory

  DIR="exp".$EXPERIMENT.$DATAFILE.`date +"%Y-%m-%d.%T"`

  mkdir $DIR
  mv *.tmp results.csv $DIR
  
done

