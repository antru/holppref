
source conf.cfg

EXPERIMENT="optim"

DEPENDENCIES="pathprefopt"

SOURCEDIR="../"

DATAFILES="
graph_10_10
graph_10_20
"

CALLS="
naive_shortest(a,z,C)
dag_shortest(a,z,C)
gen_shortest(a,z,C,1000)
"

TIMEOUT=7200

###############################################################################

rm $SOURCEDIR/src/*.xwam
rm $SOURCEDIR/data/*.xwam

for DATAFILE in `echo $DATAFILES`
do
  # initialize consult string
  
  echo -n "[" > consult.tmp
  echo "$DEPENDENCIES" | xargs -I{} echo -n "'"$SOURCEDIR"src/"{}"'," >> consult.tmp
  echo "'"$SOURCEDIR"data/"$DATAFILE"']." >> consult.tmp
  
  echo 'Experiment,Datafile,Call,Time,Cost' > results.csv

  for CALL in `echo $CALLS`
  do
    START=$(($(date +%s%N)/1000000))

    echo $CALL",nl,write(C),nl,!." > command.tmp
    cat consult.tmp command.tmp | timeout $TIMEOUT $XSB_PATH | tee $CALL.output.tmp

    END=$(($(date +%s%N)/1000000))

    TIME=$(($END-$START))
    LENGTH=`head -n 3 $CALL.output.tmp | tail -n 1`
    
    CALLPRINT=`echo $CALL | tr ",;" "_"`

    echo $EXPERIMENT","$DATAFILE","$CALLPRINT","$TIME","$LENGTH >> results.csv
  done
  
  # move to a backup directory

  DIR="path".$EXPERIMENT.$DATAFILE.`date +"%Y-%m-%d.%T"`

  mkdir $DIR
  mv *.tmp results.csv $DIR
  
done

