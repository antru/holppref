
source conf.cfg

EXPERIMENT="all"

DEPENDENCIES="
../../src/basics
../../src/preferences
../../src/subsetof
../../src/optimizations
setpref"

SOURCEDIR="../"

DATAFILES="
movie_10
movie_25
movie_50
movie_75
movie_100
"

CALLS="
winnow(ranking_pref,subsetof(movie,3))
winnow(opt_ranking_pref,subsetof(movie,3))
winnowsuper(ranking_pref,ranking_superpref,movie,3)
winnowmrelation(ranking_pref,ranking_mrel,movie,3)
"

###############################################################################

rm $SOURCEDIR/src/*.xwam
rm $SOURCEDIR/data/*.xwam

for DATAFILE in `echo $DATAFILES`
do
  # initialize consult string
  
  echo -n "[" > consult.tmp
  echo "$DEPENDENCIES" | xargs -I{} echo -n "'"$SOURCEDIR"src/"{}"'," >> consult.tmp
  echo "'"$SOURCEDIR"data/"$DATAFILE"']." >> consult.tmp
  
  echo 'Experiment,Datafile,Call,Time,Results,Sets' > results.csv

  for CALL in `echo $CALLS`
  do
    START=$(($(date +%s%N)/1000000))

    echo $CALL"(X),write(X),nl,fail." > command.tmp
    cat consult.tmp command.tmp | $XSB_PATH | tee $CALL.output.tmp

    END=$(($(date +%s%N)/1000000))

    TIME=$(($END-$START))
    RESULTS=$(($(cat $CALL.output.tmp | wc -l)-2))
    
    CALLPRINT=`echo $CALL | tr ",;" "_"`
    
    echo $CALL"(X),fail." > set.command.tmp
    echo "ext1(X),write(X),nl,fail." >> set.command.tmp
    cat consult.tmp set.command.tmp | $XSB_PATH | tee $CALL.set.output.tmp
    
    SETS=$(($(cat $CALL.set.output.tmp | wc -l)-3))

    echo $EXPERIMENT","$DATAFILE","$CALLPRINT","$TIME","$RESULTS","$SETS >> results.csv
  done
  
  # move to a backup directory

  DIR="set".$EXPERIMENT.$DATAFILE.`date +"%Y-%m-%d.%T"`

  mkdir $DIR
  mv *.tmp results.csv $DIR
  
done

