
source conf.cfg

EXPERIMENT="optim"

DEPENDENCIES="operprefopt"

SOURCEDIR="../"

DATAFILES="
movie_100
movie_500
movie_1000
movie_2000
"

CALLS="
w(1,X)
wt(1,X)
w(2,X)
wt(2,X)
w(3,X)
wt(3,X)
w(4,X)
wt(4,X)
"

###############################################################################

rm $SOURCEDIR/src/*.xwam
rm $SOURCEDIR/data/*.xwam

for DATAFILE in `echo $DATAFILES`
do
  # initialize consult string
  
  echo -n "[" > consult.tmp
  echo "$DEPENDENCIES" | xargs -I{} echo -n "'"$SOURCEDIR"src/"{}"'," >> consult.tmp
  echo "'"$SOURCEDIR"data/"$DATAFILE"']." >> consult.tmp
  
  echo 'Experiment,Datafile,Call,Time,Results' > results.csv

  for CALL in `echo $CALLS`
  do
    START=$(($(date +%s%N)/1000000))

    echo $CALL",write(X),nl,fail." > command.tmp
    cat consult.tmp command.tmp | $XSB_PATH | tee $CALL.output.tmp

    END=$(($(date +%s%N)/1000000))

    TIME=$(($END-$START))
    RESULTS=$(($(cat $CALL.output.tmp | wc -l)-2))
    
    CALLPRINT=`echo $CALL | tr ",;" "_"`

    echo $EXPERIMENT","$DATAFILE","$CALLPRINT","$TIME","$RESULTS >> results.csv
  done
  
  # move to a backup directory

  DIR="ops".$EXPERIMENT.$DATAFILE.`date +"%Y-%m-%d.%T"`

  mkdir $DIR
  mv *.tmp results.csv $DIR
  
done

