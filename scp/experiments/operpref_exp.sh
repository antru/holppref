
source conf.cfg

EXPERIMENT="naive"

DEPENDENCIES="operpref"

SOURCEDIR="../"

DATAFILES="
movie_100
movie_500
movie_1000
movie_2000
"

CALLS="
w(c1_pref,movie)(1)
wt(c1_pref,movie)(1)
w(c1_pref,movie)(2)
wt(c1_pref,movie)(2)
w(c1_pref,movie)(3)
wt(c1_pref,movie)(3)
w(c1_pref,movie)(4)
wt(c1_pref,movie)(4)
"

TIMEOUT=7200

###############################################################################

rm $SOURCEDIR/src/*.xwam
rm $SOURCEDIR/data/*.xwam

for DATAFILE in `echo $DATAFILES`
do
  # initialize consult string
  
  echo -n "[" > consult.tmp
  echo "$DEPENDENCIES" | xargs -I{} echo -n "'"$SOURCEDIR"src/"{}"'," >> consult.tmp
  echo "'"$SOURCEDIR"data/"$DATAFILE"']." >> consult.tmp
  
  echo 'Experiment,Datafile,Call,Time,Results' > results.csv

  for CALL in `echo $CALLS`
  do
    START=$(($(date +%s%N)/1000000))

    echo $CALL"(X),write(X),nl,fail." > command.tmp
    cat consult.tmp command.tmp | timeout $TIMEOUT $XSB_PATH | tee $CALL.output.tmp

    END=$(($(date +%s%N)/1000000))

    TIME=$(($END-$START))
    RESULTS=$(($(cat $CALL.output.tmp | wc -l)-2))
    
    CALLPRINT=`echo $CALL | tr ",;" "_"`

    echo $EXPERIMENT","$DATAFILE","$CALLPRINT","$TIME","$RESULTS >> results.csv
  done
  
  # move to a backup directory

  DIR="ops".$EXPERIMENT.$DATAFILE.`date +"%Y-%m-%d.%T"`

  mkdir $DIR
  mv *.tmp results.csv $DIR
  
done

