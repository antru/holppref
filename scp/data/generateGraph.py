import random
import sys

if len(sys.argv) < 3:
  print "Usage: python generateGraph.py [#length] [#height]"
  exit()
  
length = int(sys.argv[1])
height = int(sys.argv[2])

weightfrom = 1*(length+height)
weightto = 50*(length+height)

source = "a"
target = ""

for i in range(0,length):
  if (i == length - 1):
    target = "z"
  else:
    target = "b" + str(i)    
  for j in range(0,height):
    v = "v" + str(i) + str(j)
    print "edge((" + source + "," + v + "," +str(random.randrange(weightfrom,weightto)) + "))."
  for j in range(0,height):
    v = "v" + str(i) + str(j)
    print "edge((" + v + "," + target + "," +str(random.randrange(weightfrom,weightto)) + "))."
  source = target


