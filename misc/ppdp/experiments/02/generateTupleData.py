import random
import sys

def pretty_string(prefix, num, width):
  return prefix + str(num).zfill(width)
  
def make_list(prefix, size):
  width = len(str(size))
  return map(lambda num: pretty_string(prefix,num,width), range(1,size+1))

if len(sys.argv) < 4:
  print "Usage: python generateTupleData.py [#records] [#books] [#vendors]"
  exit()

records = int(sys.argv[1])-5
books = int(sys.argv[2])-3
vendors = int(sys.argv[3])-3

name = "book"
isbn = ["i0679726691","i0062059041","i0374164770"] + make_list("i",books)
vendor = ["booksForLess","lowestPrices","qualityBooks"] + make_list("v",vendors)

pricefrom = 8
priceto = 55

for x in range(1,records+1):
  res = []
  res.append(random.choice(isbn))
  res.append(random.choice(vendor))
  res.append(str(random.randrange(pricefrom,priceto)))
  print name + "((" + ",".join(res) + "))." 
