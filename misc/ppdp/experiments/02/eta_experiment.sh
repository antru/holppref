
source ../conf.cfg

DEPENDENCIES="src/basics
src/preferences
src/memoized
examples/books"

SOURCEDIR="../../"

RECORDS=100
BOOKS=50
VENDORS=3

if [ $1 != "" ]
then
  RECORDS=$1
fi

if [ $2 != "" ]
then
  BOOKS=$2
fi

if [ $3 != "" ]
then
  VENDORS=$3
fi

# initialize consult string

echo -n "[" > consult.tmp
echo "$DEPENDENCIES" | xargs -I{} echo -n "'"$SOURCEDIR{}"'," >> consult.tmp
echo "'book']." >> consult.tmp

# initialize book relation

python generateTupleData.py $RECORDS $BOOKS $VENDORS > book.P

# run experiment

START=$(($(date +%s%N)/1000000))

echo "etao(pC1,book)(X,I),write((X,I)),nl,fail." > command.tmp
cat consult.tmp command.tmp | $XSB_PATH | tee output.tmp

END=$(($(date +%s%N)/1000000))

TIME=$(($END-$START))
RESULTS=$(($(cat output.tmp | wc -l)-2))

# find maximum level

LEVEL=`tail -n 3 output.tmp | head -n 1 | awk -F ',' '{ print $4 }' | rev | colrm 1 1 | rev`

# print results

echo 'Operator;Time;Results;Records;Books;Vendors;MaxLevel' > results.csv
echo "etao(pC1_book);"$TIME";"$RESULTS";"$RECORDS";"$BOOKS";"$VENDORS";"$LEVEL >> results.csv

# move to a backup directory

DIR="tuples".$RECORDS.`date +"%Y-%m-%d.%T"`

mkdir $DIR
mv book.P book.xwam *.tmp results.csv $DIR

