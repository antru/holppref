cat tuples.*/results.csv > 1st.csv
echo 'Operator;Time;Results;Records;Books;Vendors;MaxLevel' > 1st-clear.csv
cat 1st.csv | grep -v 'Operator' >> 1st-clear.csv
csvsql --query \
  "select Operator, Records, count(Time), sum(Time)/count(Time), sum(Results)/count(Time), sum(MaxLevel)/(count(Time)*1.0) \
  from '1st-clear' group by Operator,Records;" 1st-clear.csv > experiment.02.csv
rm 1st.csv 1st-clear.csv

