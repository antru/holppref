
source ../conf.cfg

PREFERENCES="pC1
pC2
pC3
conj(pC1,pC2)
prioritized(pC2,pC1)
big_vendor_pref
big_vendor_pref_opt"

DEPENDENCIES="src/basics
src/preferences
examples/books"

SOURCEDIR="../../"

RECORDS=100
BOOKS=50
VENDORS=3

if [ $1 != "" ]
then
  RECORDS=$1
fi

if [ $2 != "" ]
then
  BOOKS=$2
fi

if [ $3 != "" ]
then
  VENDORS=$3
fi

# initialize consult string

echo -n "[" > consult.tmp
echo "$DEPENDENCIES" | xargs -I{} echo -n "'"$SOURCEDIR{}"'," >> consult.tmp
echo "'book']." >> consult.tmp

# initialize book relation

python generateTupleData.py $RECORDS $BOOKS $VENDORS > book.P

# run experiment

echo 'Preference;Time;Results;Records;Books;Vendors' > results.csv

for PREF in `echo $PREFERENCES`
do
  START=$(($(date +%s%N)/1000000))

  echo "winnow("$PREF",book)(X),write(X),nl,fail." > command.tmp
  cat consult.tmp command.tmp | $XSB_PATH | tee $PREF.output.tmp

  END=$(($(date +%s%N)/1000000))

  TIME=$(($END-$START))
  RESULTS=$(($(cat $PREF.output.tmp | wc -l)-2))
  
  PREFPRINT=`echo $PREF | tr ",;" "_"`

  echo $PREFPRINT";"$TIME";"$RESULTS";"$RECORDS";"$BOOKS";"$VENDORS >> results.csv
done

# move to a backup directory

DIR="tuples".$RECORDS.`date +"%Y-%m-%d.%T"`

mkdir $DIR
mv book.P book.xwam *.tmp results.csv $DIR

