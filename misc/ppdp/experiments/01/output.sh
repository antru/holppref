cat tuples.*/results.csv > 1st.csv
echo 'Preference;Time;Results;Records;Books;Vendors' > 1st-clear.csv
cat 1st.csv | grep -v 'Preference' >> 1st-clear.csv
csvsql --query \
  "select Preference, Records, count(Time), sum(Time)/count(Time), sum(Results)/count(Time) \
  from '1st-clear' group by Preference,Records;" 1st-clear.csv > experiment.01.csv
rm 1st.csv 1st-clear.csv
