
source ../conf.cfg

rm book.P book.xwam
rm *.tmp results.csv
rm latest

RECORDS=100
GENRES=10
SETS=3

if [ $1 != "" ]
then
  RECORDS=$1
fi

if [ $2 != "" ]
then
  GENRES=$2
fi

if [ $3 != "" ]
then
  SETSIZE=$3
fi

echo 'RECORDS =' $RECORDS
echo 'GENRES =' $GENRES
echo 'SETS =' $SETSIZE

# % :- ['src/basics','src/preferences','src/subsetof','src/optimizations','examples/booksetssimple'].
# 
# % :- winnow(price_pref,subsetof(book,3))(S),S(T).
# % :- winnowsuper(price_pref,price_superpref,book,3)(S),S(T).
# % :- winnowmrelation(price_pref,price_mrel,book,3)(S),S(T).

QUERIES="winnow(price_pref,subsetof(book,"$SETSIZE"))(S),S(T)
winnowsuper(price_pref,price_superpref,book,"$SETSIZE")(S),S(T)
winnowmrelation(price_pref,price_mrel,book,"$SETSIZE")(S),S(T)"

python generateSetData.py $RECORDS $GENRES > book.P

echo 'Query;Time;Results;Records;SetSize' > results.csv

for QUERY in $QUERIES
do
  START=$(($(date +%s%N)/1000000))

  echo "['../../src/basics', '../../src/preferences', '../../src/subsetof', \
         '../../src/optimizations', '../../examples/booksetssimple', 'book'].
        "$QUERY",write(S),write(T),nl,fail.
  " | $XSB_PATH | tee $QUERY.output.tmp

  END=$(($(date +%s%N)/1000000))

  TIME=$(($END-$START))
  RESULTS=$(($(cat $QUERY.output.tmp | wc -l)-3))
  
  PRINT=`echo $QUERY | tr ",;" "_"`

  echo $PRINT";"$TIME";"$RESULTS";"$RECORDS";"$SETSIZE>> results.csv
done

DIR="sets".$RECORDS.$SETSIZE.`date +"%Y-%m-%d.%T"`

mkdir $DIR
mv book.P book.xwam *.tmp results.csv $DIR
ln -s $DIR latest
