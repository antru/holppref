import random
import sys

def pretty_string(prefix, num, width):
  return prefix + str(num).zfill(width)
  
def make_list(prefix, size):
  width = len(str(size))
  return map(lambda num: pretty_string(prefix,num,width), range(1,size+1))

if len(sys.argv) < 3:
  print "Usage: python generateTupleData.py [#records] [#genres]"
  exit()

records = int(sys.argv[1])
genres = int(sys.argv[2])-3

name = "book"
genre = ["scifi","biography","romance"] + make_list("g",genres)
vendor = ["amazon","bn"]

pricefrom = 5
priceto = 95

for x in range(9,records+1):
  res = []
  res.append("a"+str(x))
  res.append(random.choice(genre))
  res.append(str(random.randrange(10,51)))
  res.append(str(random.randrange(pricefrom,priceto)))
  res.append(random.choice(vendor))
  print name + "((" + ",".join(res) + "))." 
