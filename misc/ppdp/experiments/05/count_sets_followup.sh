
source ../conf.cfg

for DIR in `ls -d sets.*`
do
  cd $DIR
  
  echo ":- hilog successor.
  successor(X,_Y,Z) :- Z is X+1." > succ.P

  rm sets.output.tmp
  
  SETSIZE=`head -n 2 results.csv | tail -n 1 | awk -F ';' '{ print $5 }'`

  echo "['../../../src/basics', '../../../src/preferences', '../../../src/subsetof', \
	'../../../src/optimizations', '../../../examples/booksetssimple', 'succ', 'book'].
	import bagCount/2 from aggregs.
	winnow(price_pref,subsetof(book,"$SETSIZE"))(S),write(S),write(T),nl,fail.
	ext0(subsetof(book,"$SETSIZE"),X),bagCount(X,N),write('Naive: Sets = '),write(N),nl,fail.
  " | $XSB_PATH >> sets.output.tmp
  
  echo "['../../../src/basics', '../../../src/preferences', '../../../src/subsetof', \
	'../../../src/optimizations', '../../../examples/booksetssimple', 'succ', 'book'].
	import bagCount/2 from aggregs.
	winnowsuper(price_pref,price_superpref,book,"$SETSIZE")(S),write(S),write(T),nl,fail.
	ext0(sprf_subsetof(prunerel(book,price_superpref,"$SETSIZE"),price_superpref,"$SETSIZE"),X),bagCount(X,N),write('Super: Sets = '),write(N),nl,fail.
  " | $XSB_PATH >> sets.output.tmp
  
  echo "['../../../src/basics', '../../../src/preferences', '../../../src/subsetof', \
	'../../../src/optimizations', '../../../examples/booksetssimple', 'succ', 'book'].
	import bagCount/2 from aggregs.
	winnowmrelation(price_pref,price_mrel,book,"$SETSIZE")(S),write(S),write(T),nl,fail.
	ext0(mrel_multisubsetof(mrel_mrelation(book,price_mrel),"$SETSIZE"),X),bagCount(X,N),write('Mrel: Sets = '),write(N),nl,fail.
  " | $XSB_PATH >> sets.output.tmp

  grep Sets sets.output.tmp | colrm 1 5 > count.sets.txt
  
  cd ..
done

