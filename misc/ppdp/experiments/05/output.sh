cat sets.*/results.csv > 1st.csv
echo 'Query;Time;Results;Records;SetSize' > 1st-clear.csv
cat 1st.csv | grep -e '0' -e '5' >> 1st-clear.csv
csvsql --query \
  "select Query, Records, SetSize, count(Time), sum(Time)/count(Time), sum(Results)/count(Time)
  from '1st-clear' group by Query,Records,SetSize;" 1st-clear.csv > experiment.05.csv

echo 'Method;Records;SetSize;Sets' > sets-tmp.csv
ls -d sets.* | xargs -I {} sed 's/: Sets = /.{}./g' {}/count.sets.txt | awk -F '.' '{ print $1";"$3";"$4";"$7 }' >> sets-tmp.csv
csvsql --query \
  "select Method, Records, SetSize, sum(Sets)/(count(Sets)*1.0)
  from 'sets-tmp' group by Method,Records,SetSize;" sets-tmp.csv > setcount.05.csv

rm 1st.csv 1st-clear.csv sets-tmp.csv
