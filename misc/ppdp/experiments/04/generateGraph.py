import random
import sys

if len(sys.argv) < 2:
  print "Usage: python generateGraph.py [#n]"
  exit()
  
n = int(sys.argv[1])

print ":- hilog node, edge."

for i in range(0,n):
  for j in range(0,n):
    print "node(v_" + str(i) + "_" + str(j) + ")."

weightfrom = 8
weightto = 55

for i in range(0,n):
  for j in range(0,n):
    rand = random.random()
    v0 = "v_" + str(i) + "_" + str(j)
    if i < n-1:
      v1 = "v_" + str(i+1) + "_" + str(j)
      print "edge((" + v0 + "," + v1 + "," +str(random.randrange(weightfrom,weightto)) + "))."
    if j < n-1:
      v2 = "v_" + str(i) + "_" + str(j+1)
      print "edge((" + v0 + "," + v2 + "," +str(random.randrange(weightfrom,weightto)) + "))."

