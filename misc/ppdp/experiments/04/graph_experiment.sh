
source ../conf.cfg

PREFERENCES="path_pref,path
tpath_pref,tpath"

DEPENDENCIES="src/basics
src/preferences
src/memoized
examples/simple"

SOURCEDIR="../../"

NODES=2

if [ $1 != "" ]
then
  NODES=$1
fi

# initialize consult string

echo -n "[" > consult.tmp
echo "$DEPENDENCIES" | xargs -I{} echo -n "'"$SOURCEDIR{}"'," >> consult.tmp
echo "'graph']." >> consult.tmp

# generate graph (edge and node) relations

python generateGraph.py $NODES > graph.P

# run experiment

echo 'Query;Time;Results;Nodes' > results.csv

for PREF in `echo $PREFERENCES`
do
  START=$(($(date +%s%N)/1000000))

  echo "winnow("$PREF")(X),write(X),nl,fail." > command.tmp
  cat consult.tmp command.tmp | $XSB_PATH | tee $PREF.output.tmp

  END=$(($(date +%s%N)/1000000))

  TIME=$(($END-$START))
  RESULTS=$(($(cat $PREF.output.tmp | wc -l)-2))
  
  PREFPRINT=`echo $PREF | tr ",;" "_"`

  echo "winnow("$PREFPRINT");"$TIME";"$RESULTS";"$(($NODES*$NODES)) >> results.csv
done

# move to a backup directory

DIR="graph".$NODES.`date +"%Y-%m-%d.%T"`

mkdir $DIR
mv graph.P graph.xwam *.tmp results.csv $DIR
