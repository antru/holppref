cat graph.*/results.csv > 1st.csv
echo 'Query;Time;Results;Nodes' > 1st-clear.csv
cat 1st.csv | grep -v 'Query' >> 1st-clear.csv
csvsql --query \
  "select Query, Nodes, count(Time), sum(Time)/count(Time), sum(Results)/count(Time) 
  from '1st-clear' group by Query,Nodes;" 1st-clear.csv > experiment.04.csv
rm 1st.csv 1st-clear.csv

