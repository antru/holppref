cat *tuples*/results.csv > 1st.csv
echo 'Operator;Time;Results;Records;Books;Vendors' > 1st-clear.csv
cat 1st.csv | grep 0 >> 1st-clear.csv
csvsql --query \
  "select Operator, Records, count(Time), sum(Time)/count(Time), sum(Results)/count(Time) \
  from '1st-clear' group by Records,Operator;" 1st-clear.csv  > output-tmp.csv
sed \
  -e 's/Operator/Operator,Key/g' \
  -e 's/w(pC1_book)(1),/w(pC1_book)(1),1,/g' \
  -e 's/w(pC1_book)(2),/w(pC1_book)(2),2,/g' \
  -e 's/w(pC1_book)(3),/w(pC1_book)(3),3,/g' \
  -e 's/w(pC1_book)(4),/w(pC1_book)(4),4,/g' \
  -e 's/wt(pC1_book)(1),/wt(pC1_book)(1),6,/g' \
  -e 's/wt(pC1_book)(2),/wt(pC1_book)(2),7,/g' \
  -e 's/wt(pC1_book)(3),/wt(pC1_book)(3),8,/g' \
  -e 's/wt(pC1_book)(4),/wt(pC1_book)(4),9,/g' \
  -e 's/wo(pC1_book)(1),/wo(pC1_book)(1),11,/g' \
  -e 's/wo(pC1_book)(2),/wo(pC1_book)(2),12,/g' \
  -e 's/wo(pC1_book)(3),/wo(pC1_book)(3),13,/g' \
  -e 's/wo(pC1_book)(4),/wo(pC1_book)(4),14,/g' \
  -e 's/wto(pC1_book)(1),/wto(pC1_book)(1),16,/g' \
  -e 's/wto(pC1_book)(2),/wto(pC1_book)(2),17,/g' \
  -e 's/wto(pC1_book)(3),/wto(pC1_book)(3),18,/g' \
  -e 's/wto(pC1_book)(4),/wto(pC1_book)(4),19,/g' \
  -e 's/winnow(pC1_book),/winnow(pC1_book),0,/g' output-tmp.csv > experiment.03.csv
rm 1st.csv 1st-clear.csv output-tmp.csv

