
source ../conf.cfg

ls -d tuples.* | xargs -I{} cp -r {} optimized.{}

OPERATORS="wo(pC1,book)(1)
wto(pC1,book)(1)
wo(pC1,book)(2)
wto(pC1,book)(2)
wo(pC1,book)(3)
wto(pC1,book)(3)
wo(pC1,book)(4)
wto(pC1,book)(4)"

for DIR in `ls -d optimized.tuples.*`
do
  cd $DIR
  
  RECORDS=`head -n 2 results.csv | tail -n 1 | awk -F ';' '{ print $4 }'`
  BOOKS=`  head -n 2 results.csv | tail -n 1 | awk -F ';' '{ print $5 }'`
  VENDORS=`head -n 2 results.csv | tail -n 1 | awk -F ';' '{ print $6 }'`
  
  rm *.tmp *.xwam results.csv
  
  echo 'Operator;Time;Results;Records;Books;Vendors' > results.csv
  
  for GOAL in $OPERATORS
  do
    START=$(($(date +%s%N)/1000000))

    echo "['../../../src/basics','../../../src/preferences','../../../src/memoized','../../../examples/books','book'].
	  $GOAL(X),write(X),nl,fail.
    " | $XSB_PATH | tee $GOAL.output.tmp

    END=$(($(date +%s%N)/1000000))

    TIME=$(($END-$START))
    RESULTS=$(($(cat $GOAL.output.tmp | wc -l)-3))
    
    GOALPRINT=`echo $GOAL | tr ",;" "_"`

    echo $GOALPRINT";"$TIME";"$RESULTS";"$RECORDS";"$BOOKS";"$VENDORS >> results.csv
  done
  cd ..
done

