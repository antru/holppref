
source ../conf.cfg

rm book.P book.xwam
rm *.tmp results.csv
rm latest

RECORDS=1000
BOOKS=500
VENDORS=10
MAXN=1

if [ $1 != "" ]
then
  RECORDS=$1
fi

if [ $2 != "" ]
then
  BOOKS=$2
fi

if [ $3 != "" ]
then
  VENDORS=$3
fi

if [ $4 != "" ]
then
  MAXN=$4
fi

echo 'RECORDS =' $RECORDS
echo 'BOOKS =' $BOOKS
echo 'VENDORS =' $VENDORS

OPERATORS="winnow(pC1,book)
w(pC1,book)(1)
wt(pC1,book)(1)
w(pC1,book)(2)
wt(pC1,book)(2)
w(pC1,book)(3)
wt(pC1,book)(3)
w(pC1,book)(4)
wt(pC1,book)(4)"

LINES=`expr $MAXN \* 2 + 1`
echo "$OPERATORS" | head -n $LINES

python generateTupleData.py $RECORDS $BOOKS $VENDORS > book.P

echo 'Operator;Time;Results;Records;Books;Vendors' > results.csv

for GOAL in `echo "$OPERATORS" | head -n $LINES`
do
  START=$(($(date +%s%N)/1000000))

  echo "['../../src/basics','../../src/preferences','../../examples/books','book'].
        $GOAL(X),write(X),nl,fail.
  " | $XSB_PATH | tee $GOAL.output.tmp

  END=$(($(date +%s%N)/1000000))

  TIME=$(($END-$START))
  RESULTS=$(($(cat $GOAL.output.tmp | wc -l)-3))
  
  GOALPRINT=`echo $GOAL | tr ",;" "_"`

  echo $GOALPRINT";"$TIME";"$RESULTS";"$RECORDS";"$BOOKS";"$VENDORS >> results.csv
done

DIR="tuples".$RECORDS.`date +"%Y-%m-%d.%T"`

mkdir $DIR
mv book.P book.xwam *.tmp results.csv $DIR
ln -s $DIR latest
