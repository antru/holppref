% Book tuple preferences.
% File: books.P
% Language: Hilog (xsb)
% Author: Antonis Troumpoukis
% Contact: antru@di.uoa.gr

:- hilog  book, pC1, pC2, pC3, big_vendor_pref, vendor, books_of_vendor,
          big_vendor_pref_opt, vendor_opt, vendor_mem.

%
% Example queries.
%

% :- ['src/basics','src/preferences','examples/books'].

% :- winnow(pC1,book)(X).
% :- winnow(prioritized(pC2,pC1),book)(X).
% :- winnow(big_vendor_pref,book)(X).

% :- w(pC1,book)(2)(X).
% :- wt(pC1,book)(2)(X).
% :- eta(pC1,book)(X,I).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Book Relation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% book((ISBN, Vendor, Price)).
%

book(( i0679726691, booksForLess, 14 )).
book(( i0679726691, lowestPrices, 13 )).
book(( i0679726691, qualityBooks, 18 )).
book(( i0062059041, booksForLess,  7 )).
book(( i0374164770, lowestPrices, 21 )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Book Preferences
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Prefer one book tuple to another if and only if their ISBNs are the same 
% and the price of the first is lower.
%

pC1((I,V1,P1), (I,V2,P2)) :- book((I,V1,P1)), book((I,V2,P2)), P1<P2.

%
% I prefer to buy one book from booksForLess vendor to lowestPrices vendor.
%

pC2((I,booksForLess,P1), (I,lowestPrices,P2)) :-
  book((I,booksForLess,P1)), book((I,lowestPrices,P2)).

%
% I prefer to buy one book from qualityBooks than any other vendor.
%

pC3((I,qualityBooks,P1), (I,V,P2)) :-
  book((I,qualityBooks,P1)), book((I,V,P2)), not (V=qualityBooks).


%
% I prefer to buy books from the vendor that sells most books.
%

big_vendor_pref((I,V1,P1), (I,V2,P2)) :- 
  book((I,V1,P1)), book((I,V2,P2)), vendor(V1,N1), vendor(V2,N2), N1>N2.

vendor(V,N) :- book((_,V,_)), !, size(books_of_vendor(V),N).
books_of_vendor(V)(I) :- book((I,V,_)).

%
% Optimized version
%

big_vendor_pref_opt((I,V1,P1), (I,V2,P2)) :- 
  book((I,V1,P1)), book((I,V2,P2)), vendor_opt(V1,N1), vendor_opt(V2,N2), N1>N2.

vendor_opt(V,N) :- vendor_mem(V,N).
vendor_opt(V,N) :- not vendor_mem(V,N), vendor(V,N), assert(vendor_mem(V,N)).

