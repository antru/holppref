% Preferences over collections of books.
% File: booksets.P
% Dependencies: preferences.P
% Language: Hilog (xsb)
% Author: Antonis Troumpoukis
% Contact: antru@di.uoa.gr

%
% Example Queries.
%

% :- ['src/basics','src/preferences','src/subsetof','src/optimizations','examples/booksets'].

% Preference 1), see below.
% :- winnow(price_pref,subsetof(book,3))(S),S(T).
% :- winnowsuper(price_pref,price_superpref,book,3)(S),S(T).
% :- winnowmrelation(price_pref,price_mrel,book,3)(S),S(T).

% Preference 3), see below.
% :- winnow(vendor_pref,subsetof(book,3))(S),S(T).

% Preference 4), see below.
% :- winnow(pricescifi_pref,subsetof(book,3))(S),S(T).

% Preference 7), see below.
% :- winnow(goodscifi_pref,subsetof(book,3))(S),S(T).
% :- winnowsuper(goodscifi_pref,goodscifi_superpref,book,3)(S),S(T).
% :- winnow(goodscifi_pref,subsetof(bookext,3))(S),S(T).
% :- winnowmrelation(goodscifi_pref,goodscifi_mrel,bookext,3)(S),S(T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Book Relation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% book((ID, Genre, Rating, Price, Vendor)).
%

:- hilog  book, bookext, book_genre, book_stars, book_price, book_vendr, less.

book_genre( (_,Genre,_,_,_), Genre ).
book_stars( (_,_,Stars,_,_), Stars ).
book_price( (_,_,_,Price,_), Price ).
book_vendr( (_,_,_,_,Vendr), Vendr ).

book(( a1, scifi,     50,  15, amazon )).
book(( a2, biography, 48,  20, bn     )).
book(( a3, scifi,     45,  25, amazon )).
book(( a4, romance,   44, 100, bn     )).
book(( a5, scifi,     43,  15, amazon )).
book(( a6, romance,   42,  12, bn     )).
book(( a7, biography, 40,  18, amazon )).
book(( a8, scifi,     35,  18, amazon )).

% bug XSB?  14.2000 is 9.8000 + 4.4000.

% add two more tuples in book (for m-relation optimization).

bookext(X) :- book(X).
bookext((  a9, romance, 40, 20, amazon )).
bookext(( a10, history, 40, 19, amazon )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Book Preferences
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- hilog  feature_less, feature_value, 
          price_pref, price,
          scifi_pref, scifi_sum, scifi_books,
          vendor_pref, vendor_sum, projection, 
          pricescifi_pref, 
          scifi_price_pref, scifi_price,
          rating_pref, rating_sum, 
          goodscifi_pref.

%
% Compare two collections by comparing some feature of them.
%

feature_less(Feature)(Col1,Col2) :- Feature(Col1,X1), Feature(Col2,X2), X1 < X2.

%
% One collection is prefered if the feature returns a desired value.
%

feature_value(Feature,Val)(Col1,Col2) :- Feature(Col1,Val), not Feature(Col2,Val).

%
% 1) Spend as little money as possible.
%

price_pref(Col1,Col2) :- feature_less(price)(Col1,Col2).
price(Col,N) :- sum(Col,book_price,N).

%
% 2) Get exactly one sci-fi book.
%

scifi_pref(Col1,Col2) :- feature_value(scifi_sum,1)(Col1,Col2).
scifi_sum(Col,N) :- size(scifi_books(Col),N).
scifi_books(Col)(X) :- Col(X), book_genre(X,scifi).

%
% 3) Get books from as little vendors as possible.
%

vendor_pref(Col1,Col2) :- feature_less(vendor_sum)(Col1,Col2).
vendor_sum(Col,N) :- size(projection(Col,book_vendr),N).

projection(Col,P)(A) :- Col(X),P(X,A).

%
% 4) Prioritize 1) to 2).
%

pricescifi_pref(Col1, Col2) :- prioritized(scifi_pref, price_pref)(Col1, Col2).

%
% 5) Spend as little money as possible on sci-fi books.
%

scifi_price_pref(Col1,Col2) :- feature_less(scifi_price)(Col1,Col2).
scifi_price(Col,N) :- sum(scifi_books(Col),book_price,N).

%
% 6) Select the books with the highest rating.
%

rating_pref(Col1,Col2) :- feature_less(rating_sum)(Col2,Col1).
rating_sum(Col,N) :- sum(Col,book_stars,N).

%
% 7) Both 5) and 6).
%

goodscifi_pref(Col1, Col2) :- conj(scifi_price_pref,rating_pref)(Col1, Col2).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Book "Super-Preferences"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- hilog  price_superpref, goodscifi_superpref.
    
%
% Superpreference for 1).
%

price_superpref(B1,B2) :- book_price(B1,X1), book_price(B2,X2), X1 < X2.

%
% Superpreference for 7).
%

goodscifi_superpref(B1,B2) :- 
  book_stars(B1,R1), book_stars(B2,R2), 
  book_genre(B1,G1), book_genre(B2,G2),
  book_price(B1,P1), book_price(B2,P2),
  R1 > R2, G2=scifi, (not(G1=scifi) ; P1 < P2).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Book M-relations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- hilog  convert, price_mrel, goodscifi_mrel.

%
% Define the m-relation tuples, i.e. book tuples that do not contain the id.
%

book_genre( m(Genre,_,_,_), Genre ).
book_stars( m(_,Stars,_,_), Stars ).
book_price( m(_,_,Price,_), Price ).
book_vendr( m(_,_,_,Vendr), Vendr ).

%
% M-relation for 1).
%

price_mrel( (_,_,_,Price,_), m(null,null,Price,null) ).

%
% M-relation for 7).
%

goodscifi_mrel( (_,scifi,S,Price,_), m(scifi,S,Price,null)   ).
goodscifi_mrel( (_,Genre,S,_,_),     m(notscifi,S,null,null) ) :- not Genre=scifi.

